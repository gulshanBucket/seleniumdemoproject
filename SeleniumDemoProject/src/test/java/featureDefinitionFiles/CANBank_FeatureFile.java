package featureDefinitionFiles;


import cucumber.api.java.After;
import cucumber.api.java.Before;
import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;
import keywordFiles.CANBank_KeywordsFile;
import util.CommonMethods;

public class CANBank_FeatureFile {
	private CANBank_KeywordsFile canBankKeyword = null;
	private CommonMethods comm = null;
	public CANBank_FeatureFile() {
		canBankKeyword = new CANBank_KeywordsFile();
		comm = new CommonMethods();
	}
	/**
	 * This method initiates a browser instance for test execution
	 * @param browserType
	 * @return 
	 */
    @Before
    public void setUp(){
    	System.out.println("In Before");
    }
    
    @After
    public void reset(){
    	System.out.println("In after");
    	comm.teardown();
    }
	
    
	@Given("^The user has launched \"([^\"]*)\" browser in \"([^\"]*)\"$")
	public void the_user_has_launched_browser_in(String BrowserName, String platform) throws Throwable {
		canBankKeyword.launhDesiredBrowser(BrowserName, platform);
	}

	@Given("^The customer launches CAN bank application$")
	public void the_customer_launches_CAN_bank_application() throws Throwable {
		canBankKeyword.launchApplication();
	}

	@When("^The customer navigates to business banking option$")
	public void the_customer_navigates_to_business_banking_option() throws Throwable {
		canBankKeyword.navigateToBussinessBanking();
	}

	@When("^The customer checks for the Coronavirus support$")
	public void the_customer_checks_for_the_Coronavirus_support() throws Throwable {
		canBankKeyword.verifyBusinessCovidSupport();
	}

	@When("^The customer clicks on Login button for internet banking$")
	public void the_customer_clicks_on_Login_button_for_internet_banking() throws Throwable {
		canBankKeyword.navigateToNetbanking();
	}

	@Then("^The customer is displayed the login page$")
	public void the_customer_is_displayed_the_login_page() throws Throwable {

	}
}
