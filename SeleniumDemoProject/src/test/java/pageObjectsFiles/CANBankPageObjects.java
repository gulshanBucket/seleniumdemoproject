package pageObjectsFiles;

import org.openqa.selenium.By;

import util.SuperClass;

public class CANBankPageObjects extends SuperClass{

	private By businessOption = By.xpath("//a[text()='Business']");
	private By cbaBanner = By.id("CB-BUS-HEROBANNER");
	private By coronavirusSupportBtn = By.xpath("//a[text()='Coronavirus support']");
	private By businessSupportSection= By.id("Business");
	private By takeALoanSection = By.cssSelector("#Business > div > div > div > div > div.content-section > div > div:nth-child(1)");
	private By carAndEquipment = By.cssSelector("#Business > div > div > div > div > div.content-section > div > div:nth-child(2)");
	private By moreAboutThisLoan = By.xpath("//*[starts-with(@id,'Business')] //a[text()='More about this loan']");
	
	private By loginBtn = By.xpath("//*[starts-with(@class,'commbank-header-module')] //li[2]/a/span[text()='Log on']");
	private By netBankinglogin = By.xpath("//a[text()='NetBank log on ']");
	private By username = By.id("txtMyClientNumber_field");
	private By password = By.id("txtMyPassword_field");
	
	
	public void clickBusinessOption() throws InterruptedException{
		waitforElementClickable(businessOption);
		clickElement(businessOption);
	}
	
	public String getCBABannerText() throws InterruptedException{
		explicitWaitforElement(cbaBanner);
		return getTextFromElement(cbaBanner);
	}
	
	public void clickCovidSupportBtn() throws InterruptedException{
		clickElement(coronavirusSupportBtn);
	}
	
	public boolean isBusinessSuportDisplayed() throws InterruptedException{
		explicitWaitforElement(businessOption);
		return isElementDisplayed(businessSupportSection);
	}
	
	public String getTextTakeALoanSection(){
		explicitWaitforElement(takeALoanSection);
		String value = getTextFromElement(takeALoanSection);
		return value;
	}
	
	public String getTextCarAndEquipmentSection(){
		String value = getTextFromElement(carAndEquipment);
		return value;
	}
	
	public void clickLoginBtn() throws InterruptedException{
		clickElement(loginBtn);
	}
	
	public void clickNetBankingLoginBtn() throws InterruptedException{
		explicitWaitforElement(netBankinglogin);
		clickElement(netBankinglogin);
	}
	
	public boolean isUsernameDisplayed() throws InterruptedException{
		explicitWaitforElement(username);
		return isElementDisplayed(username);
	}
	
	public boolean isPasswordDisplayed() throws InterruptedException{
		explicitWaitforElement(password);
		return isElementDisplayed(password);
	}
	
	public void clickMoreAboutThisLoan() throws InterruptedException{
		clickElement(moreAboutThisLoan);
	}
}
