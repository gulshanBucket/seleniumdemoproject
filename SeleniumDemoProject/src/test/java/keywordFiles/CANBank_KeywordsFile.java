package keywordFiles;

import static org.testng.Assert.assertTrue;

import java.net.MalformedURLException;

import org.junit.Assert;

import dataFiles.ProjectDataClass;
import pageObjectsFiles.CANBankPageObjects;
import util.CommonMethods;

public class CANBank_KeywordsFile {

	private CANBankPageObjects canBankPages = null;
	private ProjectDataClass dataClass = null;
	private CommonMethods comm = null;

	public CANBank_KeywordsFile() {
		dataClass = ProjectDataClass.getInstance();
		comm = new CommonMethods();
		canBankPages = new CANBankPageObjects();
	}

	/**
	 * Method to launch Browser
	 * 
	 * @throws MalformedURLException
	 * @throws Exception
	 */
	public void launhDesiredBrowser(String BrowserName, String platform) throws MalformedURLException {
		comm.launchBrowser(BrowserName, platform);
	}

	/**
	 * Method to launch application
	 * 
	 * @throws Exception
	 */
	public void launchApplication() throws Exception {
		comm.launchURL(dataClass.getCanBankUrl());
		comm.getScreenshot("Application_HomePage");
	}

	/**
	 * Method to navigate to business banking
	 * 
	 * @throws Exception
	 */
	public void navigateToBussinessBanking() throws Exception {
		canBankPages.clickBusinessOption();

		String expectedHeadline = "Business banking";
		String actualBannerText = canBankPages.getCBABannerText();

		if (!actualBannerText.contains(expectedHeadline))
			Assert.fail("Expected Business Section Header " + expectedHeadline
					+ " isn't displayed in the Business section");

		comm.getScreenshot("BusinessBankingPage");
	}

	/**
	 * Method to verify business covid support page
	 * 
	 * @throws InterruptedException
	 */
	public void verifyBusinessCovidSupport() throws InterruptedException {
		canBankPages.clickCovidSupportBtn();

		String expectedTakeNewLoanText = "Take a new loan";
		assertTrue(canBankPages.getTextTakeALoanSection().contains(expectedTakeNewLoanText));

		String expectedCarAndEquipmentText = "Car & equipment finance";
		assertTrue(canBankPages.getTextCarAndEquipmentSection().contains(expectedCarAndEquipmentText));

		canBankPages.clickMoreAboutThisLoan();
	}

	/**
	 * Method to navigate to Netbanking page
	 * 
	 * @throws Exception
	 */
	public void navigateToNetbanking() throws Exception {
		canBankPages.clickLoginBtn();
		canBankPages.clickNetBankingLoginBtn();
	}

	public void verifyNetbankingPageDisplayed() throws Exception {
		assertTrue(canBankPages.isUsernameDisplayed(), "The username is displayed as expected.");
		assertTrue(canBankPages.isPasswordDisplayed(), "The password is displayed as expected.");
		comm.getScreenshot("NetbankingPage");
	}
}
