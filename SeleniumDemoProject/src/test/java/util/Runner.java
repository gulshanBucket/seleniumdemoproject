package util;


import org.junit.runner.RunWith;
import cucumber.api.CucumberOptions;
import cucumber.api.junit.Cucumber;


	@RunWith(Cucumber.class)
	@CucumberOptions(
			features = {"src/test/java/featureFiles"},
			glue={"featureDefinitionFiles"},
			tags={"@Can_Bank_BusinessBanking"},
			monochrome=true
	)


	public class Runner {

	}
