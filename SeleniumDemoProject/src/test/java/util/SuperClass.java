package util;





import java.util.ArrayList;
import java.util.List;
import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.Select;
import org.openqa.selenium.support.ui.WebDriverWait;

public abstract class SuperClass {
		
	private long shortWait = 2,longWait=30;
	private static WebDriver driver = null; 

	
	/**
	 * Method to set the driver
	 */
	public static void setDriver(WebDriver driver){
		SuperClass.driver = driver;
	}
	/**
     * Method to wait for an element for short fixed duration
     * @throws InterruptedException
     */
	public void shortWait() throws InterruptedException{
		Thread.sleep(shortWait);
	}
	
	/**
	 * Method to explicitly wait for an element
	 * @param element
	 */
	public void explicitWaitforElement(By element){
		WebDriverWait explicitWait = new WebDriverWait(driver, longWait*1000, 1000);
		  explicitWait.until(ExpectedConditions.visibilityOf(driver.findElement(element)));
	}
	
	/**
	 * Method to explicitly wait for element to invisible
	 * @param element
	 * @throws InterruptedException 
	 */
	public void waitforElementNotVisible(By element) throws InterruptedException{
		shortWait();
		WebDriverWait explicitWait = new WebDriverWait(driver, longWait*1000, 1000);
		  explicitWait.until(ExpectedConditions.invisibilityOf(driver.findElement(element)));
	}
	
	/**
	 * Method to wait for Element to be click-able
	 */
	public void waitforElementClickable(By element){
		WebDriverWait wait = new WebDriverWait(driver,longWait, 1000);
		wait.until(ExpectedConditions.elementToBeClickable(element));
	}
	
	/**
	 * Method to input values in Text fields
	 * @param element
	 * @param value
	 * @throws InterruptedException 
	 */
	public void inputValueInElement(By element,String value) throws InterruptedException{
		driver.findElement(element).sendKeys(value);
		shortWait();
	}
	
	/**
	 * Method to clear values in Text fields
	 * @param element
	 * @param value
	 * @throws InterruptedException 
	 */
	public void clearValueInElement(By element) throws InterruptedException{
		driver.findElement(element).clear();
		shortWait();
	}
	
	/**
	 * Method to select visible text from drop down
	 * @throws InterruptedException 
	 */
	public void selectVisibleTextInList(By element,String value) throws InterruptedException{
		Select selectFromDropDown = new Select(driver.findElement(element));
		selectFromDropDown.selectByVisibleText(value);
		shortWait();
	}
	
	/**
	 * Method to select values from drop down
	 * @throws InterruptedException 
	 */
	public void selectValueTextInList(By element,String value) throws InterruptedException{
		Select selectFromDropDown = new Select(driver.findElement(element));
		selectFromDropDown.selectByValue(value);
		shortWait();
	}
	
	
	/**
	 * Method to click on a button
	 * @param buttonToClick
	 * @throws InterruptedException 
	 */
	public void clickElement(By element) throws InterruptedException{
		driver.findElement(element).click();
		shortWait();
	}
	
	/**
	 * Method to get text from an element
	 * @param element
	 * @return
	 */
	public String getTextFromElement(By element){
		String value = driver.findElement(element).getText().trim();
		return value;
	}
	

	/**
	 * Method to get data from table
	 * @return
	 */
	public List<String> getTableDetails(By rowElement){
	    List<String> tableData = new ArrayList<>();		
		int size = driver.findElements(rowElement).size();
		
		for(int i=1;i<=size;i++){
		String value = driver.findElement(By.xpath(rowElement+"["+i+"]")).getText();
		tableData.add(value);
		}
		return tableData;
	}
	
	
	/**
	 * Method to get data from table
	 * @return
	 */
	public String getRowInTable(By rowElement,String Option){
	   	String index = "";
		int size = driver.findElements(rowElement).size();
		
		for(int i=1;i<=size;i++){
		String value = driver.findElement(By.xpath(rowElement+"["+i+"]")).getText();
		
		if(value.toLowerCase().contains(Option.toLowerCase())){
			index = String.valueOf(i);
			break;
		}

		}
		return index;
	}
	
	/**
	 * Method to refresh the web page
	 * @throws InterruptedException 
	 */
	public void refreshPage() throws InterruptedException{
		driver.navigate().refresh();
		shortWait();
	}
	
	/**
	 * Method to get the shadow root WebElement
	 * @param element
	 * @return
	 */
		public WebElement expandRootElement(By locator) {
			WebElement element = driver.findElement(locator);
			WebElement expectedElement = (WebElement) ((JavascriptExecutor) driver).executeScript("return arguments[0].shadowRoot",element);
			return expectedElement;
		}
		
		/**
		 * Method to get the current window handle
		 */
		public ArrayList<String>  getCurrentWindowHandle(){
			ArrayList<String>  windowHandles = new ArrayList<String>(driver.getWindowHandles());
			return windowHandles;
		}
		
		/**
		 * Method to switch to current window
		 */
		public void switchToWindow(String windowHandle){
			 driver.switchTo().window(windowHandle);
		}
		
		/**
		 * Method to verify if the given element is displayed
		 * @throws InterruptedException 
		 */
		public boolean isElementDisplayed(By locator) throws InterruptedException{
		
			shortWait();
		
		try{
				boolean value = driver.findElement(locator).isDisplayed();
				return value;
			}catch(Exception e){
				return false;
			}
		}
		
		/**
		 * Method to select value from the drop down using action class
		 * @param locator
		 * @throws InterruptedException 
		 */
		public void selectValueUsingActionClass(By locator) throws InterruptedException{
			// This will select NSW for now but the code can be extended to select other value
			// Given the time constraint I left it here only
			Actions act = new Actions(driver); 
			WebElement selectInput=driver.findElement(locator);
			act.click(selectInput).build().perform();
			shortWait();
			act.sendKeys(Keys.ARROW_DOWN).build().perform();
			shortWait();
			act.sendKeys(Keys.ENTER).build().perform();	

		}
		
		/**
		 * Method to scroll web Element
		 */
		public void scrollToElement(WebElement element){
			JavascriptExecutor jse = ((JavascriptExecutor) driver);
			jse.executeScript(
		            "arguments[0].scrollIntoView();", element);
		}
	
}

